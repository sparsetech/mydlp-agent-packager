@REM Add custom commands
@ECHO ON

set CURRENT_PATH=%CD%
set DIST=%CURRENT_PATH%\agent_dist

set INSTALL_TARGET_PATH=C:\Program Files\Comodo\MyDLP
IF NOT exist %INSTALL_TARGET_PATH% ( mkdir "%INSTALL_TARGET_PATH%" )

@REM ------------------------------------
@REM Stop all processes old MyDLP
sc.exe stop dlpminifilterkernel
sc.exe stop windivert

sc.exe stop mydlpservice
sc.exe delete mydlpservice

sc.exe stop dlpsvc
sc.exe delete dlpsvc

taskkill /F /T /IM MyDLP.exe
taskkill /F /T /IM MyDLPEpServ.exe
taskkill /F /T /IM wizard.exe

taskkill /F /T /IM ap.exe
taskkill /F /T /IM BackServe.exe
taskkill /F /T /IM dlpsvc.exe
taskkill /F /T /IM self_update.exe
taskkill /F /T /IM tusd.exe

taskkill /F /T /IM mailproxy.exe
taskkill /F /T /IM proxy.exe

taskkill /F /T /IM windows_exporter-0.16.0-amd64.exe

schtasks /delete /tn "MYDLP" /F
@REM ------------------------------------

sc.exe stop NC
taskkill /IM NC.exe /F /T
taskkill /IM mydlp* /F /T
taskkill /IM proxy.exe /F /T
echo "Stop all process"

xcopy /A /E /H /Y /F "%DIST%\*" "%INSTALL_TARGET_PATH%\"
echo "Export files to %INSTALL_TARGET_PATH%"

cmd /c "%INSTALL_TARGET_PATH%\configure.bat"
echo "Run configure bat"

sc.exe start NC
echo "Start NC service"

TIMEOUT /T 20
echo "Agent Install End"
exit 0