@REM Add custom commands
@ECHO ON

set CURRENT_PATH=%CD%
set DIST=%CURRENT_PATH%\agent_runtimes

set INSTALL_TARGET_PATH=C:\Program Files\Comodo\MyDLP
IF NOT exist %INSTALL_TARGET_PATH% ( mkdir "%INSTALL_TARGET_PATH%" )

echo "Export files to %INSTALL_TARGET_PATH%"
xcopy /A /E /H /Y /F "%DIST%\*" "%INSTALL_TARGET_PATH%\"

"%INSTALL_TARGET_PATH%\vc_redist.x64.exe" /install /quiet /norestart
"%INSTALL_TARGET_PATH%\dotnet-runtime-5.0.1-win-x64.exe" /install /quiet /norestart
"%INSTALL_TARGET_PATH%\aspnetcore-runtime-5.0.1-win-x64.exe" /install /quiet /norestart

TIMEOUT /T 20
echo "Runtime Install End"
exit 0