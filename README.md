# Mydlp Agent Packager

MyDLP endpoint installer cikarmak icin hazirlanan yardimci bir repo.

```
"------------------------------------------------------"
"|             packer.bat usage helper                |"
"------------------------------------------------------"
"|  -h --help       Print helper                      |"
"|  -b              Build projects [nc, dlp]          |"
"|  -d              Download thirdparty files         |"
"|  -c              Create installer [ep, runtime]    |"
"|  --dist          Create dist folder                |"
"|  --configure     Configure dist folders            |"
"|  --clean         Remove temp files                 |"
"|  --all-clean     Remove all packer created files   |"
"------------------------------------------------------"
```

```bat
$ packer.bat -d -b nc -b dlp --dist --configure -c ep -c runtime
```

```
msiexec /package setup.msi /l* log_install.txt NCHost=0.0.0.0 /qn
```