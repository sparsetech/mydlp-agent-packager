@REM sc.exe stop NC
@REM taskkill /IM NC.exe /F /T
taskkill /IM mydlp* /F /T
taskkill /IM proxy.exe /F /T

set dlpsearcher_binary=C:\Users\IEG\source\repos\build-drepo\cpp\app\dlpsearcher\dlpsearcher.exe
set dlpsearcher_build_path=C:\Users\IEG\source\repos\build-drepo\cpp\app\dlpsearcher

set mydlp_folder=C:\Program Files\Comodo\MyDLP
set dist_folder=C:\Users\IEG\source\repos\drepo\staging\external\mydlp-agent-packer\dist

copy /Y "%dlpsearcher_binary%" "%mydlp_folder%\mydlpclipboard.exe"
copy /Y "%dlpsearcher_binary%" "%mydlp_folder%\mydlpweb.exe"
copy /Y "%dlpsearcher_binary%" "%mydlp_folder%\mydlpmail.exe"
copy /Y "%dlpsearcher_binary%" "%mydlp_folder%\mydlpprinter.exe"
copy /Y "%dlpsearcher_binary%" "%mydlp_folder%\mydlpfs.exe"
copy /Y "%dlpsearcher_binary%" "%mydlp_folder%\mydlpdiscovery.exe"
copy /Y "%dlpsearcher_binary%" "%mydlp_folder%\"
copy /Y "%dlpsearcher_build_path%\*.dll" "%mydlp_folder%\"

copy /Y "%dlpsearcher_binary%" "%dist_folder%\mydlpclipboard.exe"
copy /Y "%dlpsearcher_binary%" "%dist_folder%\mydlpweb.exe"
copy /Y "%dlpsearcher_binary%" "%dist_folder%\mydlpmail.exe"
copy /Y "%dlpsearcher_binary%" "%dist_folder%\mydlpprinter.exe"
copy /Y "%dlpsearcher_binary%" "%dist_folder%\mydlpfs.exe"
copy /Y "%dlpsearcher_binary%" "%dist_folder%\mydlpdiscovery.exe"
copy /Y "%dlpsearcher_binary%" "%dist_folder%\"
copy /Y "%dlpsearcher_build_path%\*.dll" "%dist_folder%\"

@REM sc.exe start NC