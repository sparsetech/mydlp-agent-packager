@ECHO OFF

IF NOT defined VCINSTALLDIR (
    call "C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsall.bat" x86_amd64
    cd /D "%~dp0"
)

SET PWD=%CD%

SET PACKER_TMP=%TEMP%\tmp_packer
IF NOT exist %PACKER_TMP% ( mkdir "%PACKER_TMP%" )

SET DREPOROOT=%PWD%\..\..\..
SET NCROOT=%DREPOROOT%\csharp\NC\NotificationCenter

SET SETUP_PATH=%PWD%\7za_setup_files
SET ZA=%SETUP_PATH%\7za.exe

SET DLP_BUILD_PATH=%PACKER_TMP%\build-dlp
SET NC_BUILD_PATH=%PACKER_TMP%\build-nc
SET DLPUPDATER_BUILD_PATH=%PACKER_TMP%\build-dlpupdater
SET DLPSEARCHER_BUILD_PATH=%DLP_BUILD_PATH%\cpp\app\dlpsearcher

IF NOT exist %DLP_BUILD_PATH% ( mkdir "%DLP_BUILD_PATH%" )
IF NOT exist %NC_BUILD_PATH% ( mkdir "%NC_BUILD_PATH%" )
IF NOT exist %DLPUPDATER_BUILD_PATH% ( mkdir "%DLPUPDATER_BUILD_PATH%" )

SET DOWNLOAD_PATH=%PWD%\downloads
SET AGENT_DIST_PATH=%PWD%\agent_dist
SET AGENT_RUNTIMES_PATH=%PWD%\agent_runtimes

IF NOT exist %DOWNLOAD_PATH% ( mkdir "%DOWNLOAD_PATH%" )
IF NOT exist %AGENT_DIST_PATH% ( mkdir "%AGENT_DIST_PATH%" )
IF NOT exist %AGENT_RUNTIMES_PATH% ( mkdir "%AGENT_RUNTIMES_PATH%" )

:loop
IF NOT "%1"=="" (
    IF "%1"=="-h" (
        GOTO :HELP_PRINT
    )
    IF "%1"=="--help" (
        GOTO :HELP_PRINT
    )
    IF "%1"=="-d" (
        call :DOWNLOAD_THIRDPARTY
    )
    IF "%1"=="--dist-all" (
        call :CREATE_ALL_DIST
    )
    IF "%1"=="--dist" (
        call :CREATE_DIST
    )
    IF "%1"=="--configure" (
        call :CONFIGURE_INSTALLER
    )
    IF "%1"=="-b" (
        IF "%2" NEQ "nc" ( 
            IF "%2" NEQ "dlp" (
                echo "Not known project '%2'. Please select build type [nc, dlp]"
                GOTO :theend
            )
        )
        IF "%2"=="nc" (
            call :NC_BUILD
            SHIFT
        )
        IF "%2"=="dlp" (
            call :DLPSEARCHER_BUILD
            SHIFT
        )
    )
    IF "%1"=="-c" (
        IF "%2" NEQ "ep" (
            IF "%2" NEQ "runtime" (
                echo "Not known project '%2'. Please select build type [ep, runtime]"
                GOTO :theend
            )
        )
        IF "%2"=="ep" (
            call :CREATE_AGENT
            SHIFT
        )
        IF "%2"=="runtime" (
            call :CREATE_AGENT_RUNTIME
            SHIFT
        )
    )
    IF "%1"=="--clean" (
        @REM Remove temp files
        rmdir /S /Q "%PACKER_TMP%"
    )
    IF "%1"=="--all-clean" (
        @REM Remove packer files
        rmdir /S /Q "%DOWNLOAD_PATH%"
        rmdir /S /Q "%AGENT_DIST_PATH%"
        rmdir /S /Q "%AGENT_RUNTIMES_PATH%"
        @REM Remove temp files
        rmdir /S /Q "%PACKER_TMP%"
    )
    SHIFT
    GOTO :loop
)
GOTO :theend

:HELP_PRINT
echo "------------------------------------------------------"
echo "|             packer.bat usage helper                |"
echo "------------------------------------------------------"
echo "|  -h --help       Print helper                      |"
echo "|  -b              Build projects [nc, dlp]          |"
echo "|  -d              Download thirdparty files         |"
echo "|  -c              Create installer [ep, runtime]    |"
echo "|  --dist          Create dist folder                |"
echo "|  --dist-all      Create all files dist folder      |"
echo "|  --configure     Configure dist folders            |"
echo "|  --clean         Remove temp files                 |"
echo "|  --all-clean     Remove all packer created files   |"
echo "------------------------------------------------------"
GOTO :theend

:DLPSEARCHER_BUILD
ECHO "DLPSEARCHER_BUILD"

cd %DLP_BUILD_PATH%

cmake.exe -G "Ninja" -D "CMAKE_BUILD_TYPE:STRING=Release" -D "WITH_CONSOLE=1" %DREPOROOT%
cmake.exe --build . --target dlpsearcher
copy /Y "%DLPSEARCHER_BUILD_PATH%\dlpsearcher.exe" "%DLPSEARCHER_BUILD_PATH%\dlpsearcher_console.exe"

cmake.exe -G "Ninja" -D "CMAKE_BUILD_TYPE:STRING=Release" -D "WITH_CONSOLE=0" %DREPOROOT%
cmake.exe --build . --target dlpsearcher

REM "%QTROOT%\5.15.2\msvc2019_64\bin\windeployqt.exe" --release --compiler-runtime --dir "%DLPSEARCHER_BUILD_PATH%" "%DLPSEARCHER_BUILD_PATH%\dlpsearcher.exe"

cd %PWD%
EXIT /B 0

:NC_BUILD
ECHO "NC_BUILD"

dotnet publish -r win-x64 -c Release -o "%NC_BUILD_PATH%" "%NCROOT%\NotificationCenter.csproj" --self-contained
dotnet publish -r win-x64 -c Release -o "%DLPUPDATER_BUILD_PATH%" -p:PublishSingleFile=true "%NCROOT%\NotificationCenter.csproj" --no-self-contained

EXIT /B 0

:CREATE_AGENT
cd "%SETUP_PATH%"
copy /Y "agent_setup.bat" "setup.bat"
"%ZA%" a "agent_setup.7z" "%AGENT_DIST_PATH%" "setup.bat"
copy /Y /B "7zSD.sfx" + "config.txt" + "agent_setup.7z" "mydlp_setup.exe"
copy /Y "mydlp_setup.exe" "%PWD%\mydlpagentinstaller\mydlp_setup.exe"
del /S /Q "setup.bat" "agent_setup.7z" "mydlp_setup.exe"

SET AGENT_INSTALLER_BUILD_PATH=%PACKER_TMP%\build-agent-installer
IF NOT exist %AGENT_INSTALLER_BUILD_PATH% ( mkdir "%AGENT_INSTALLER_BUILD_PATH%" )
cd %AGENT_INSTALLER_BUILD_PATH%
"%QT_STATIC%\bin\qmake.exe" "%PWD%\mydlpagentinstaller\mydlpagentinstaller.pro"
"nmake.exe" -f Makefile.Release
copy /Y "%AGENT_INSTALLER_BUILD_PATH%\release\mydlpagentinstaller.exe" "%PWD%\DLPAgent-Setup.exe"
cd "%PWD%"
EXIT /B 0

:CREATE_AGENT_RUNTIME
cd "%SETUP_PATH%"
copy /Y "runtime_setup.bat" "setup.bat"
"%ZA%" a "runtime_setup.7z" "%AGENT_RUNTIMES_PATH%" "setup.bat"
copy /Y /B "7zSD.sfx" + "config.txt" + "runtime_setup.7z" "%PWD%\DLPRuntime-Setup.exe"
del /S /Q "setup.bat" "runtime_setup.7z"
cd "%PWD%"
EXIT /B 0

:CREATE_ALL_DIST
@ECHO ON
@REM copy dlpsearcher bin and libs
copy /Y "%DLPSEARCHER_BUILD_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%"
copy /Y "%DLPSEARCHER_BUILD_PATH%\dlpsearcher_console.exe" "%AGENT_DIST_PATH%"
copy /Y "%DLPSEARCHER_BUILD_PATH%\*.dll" "%AGENT_DIST_PATH%\"

@REM copy runtimes
"%QTROOT%\5.15.2\msvc2019_64\bin\windeployqt.exe" --release --compiler-runtime --dir "%AGENT_DIST_PATH%" "%AGENT_DIST_PATH%\dlpsearcher.exe"

@REM create dlp bins
copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlpfs.exe"
copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlpweb.exe"
copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlpmail.exe"
copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlpprinter.exe"
copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlpclipboard.exe"
copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlpdiscovery.exe"
copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlputil.exe"

@REM create updater
copy /Y "%NC_BUILD_PATH%\NC.exe" "%AGENT_DIST_PATH%\dlpupdater.exe"

@REM copy NC
xcopy /A /E /H /Y /F "%NC_BUILD_PATH%\*" "%AGENT_DIST_PATH%\"

@REM copy static files
xcopy /A /E /H /Y /F "%PWD%\install_datas\*" "%AGENT_DIST_PATH%\"
xcopy /A /E /H /Y /F "%DOWNLOAD_PATH%\ghostscript\" "%AGENT_DIST_PATH%\tools\"
xcopy /A /E /H /Y /F "%DOWNLOAD_PATH%\tessdata\" "%AGENT_DIST_PATH%\tessdata\"
xcopy /A /E /H /Y /F "%DOWNLOAD_PATH%\MyKeys\*" "%AGENT_DIST_PATH%\"

@REM copy proxy
copy /Y "%DOWNLOAD_PATH%\proxy.exe" "%AGENT_DIST_PATH%"

@REM copy minifilter
copy /Y "%DOWNLOAD_PATH%\drivers\dlpminifilterkernel\dlpminifilterkernel.cat" "%AGENT_DIST_PATH%"
copy /Y "%DOWNLOAD_PATH%\drivers\dlpminifilterkernel\dlpminifilterkernel.inf" "%AGENT_DIST_PATH%"
copy /Y "%DOWNLOAD_PATH%\drivers\dlpminifilterkernel\dlpminifilterkernel.sys" "%AGENT_DIST_PATH%"
copy /Y "%DOWNLOAD_PATH%\drivers\dlpminifilterkernel_win7\dlpminifilterkernel.sys" "%AGENT_DIST_PATH%\dlpminifilterkernel_win7.sys"
@ECHO OFF
EXIT /B /0

:CREATE_DIST
copy /Y "%DLPSEARCHER_BUILD_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%"
copy /Y "%DLPSEARCHER_BUILD_PATH%\*.dll" "%AGENT_DIST_PATH%\"

"%QTROOT%\5.15.2\msvc2019_64\bin\windeployqt.exe" --release --compiler-runtime --dir "%AGENT_DIST_PATH%" "%AGENT_DIST_PATH%\dlpsearcher.exe"

copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlpfs.exe"
copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlpweb.exe"
copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlpmail.exe"
copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlpprinter.exe"
copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlpclipboard.exe"
copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlpdiscovery.exe"
copy /Y "%AGENT_DIST_PATH%\dlpsearcher.exe" "%AGENT_DIST_PATH%\mydlputil.exe"

xcopy /A /E /H /Y /F "%NC_BUILD_PATH%\*" "%AGENT_DIST_PATH%\"
xcopy /A /E /H /Y /F "%PWD%\install_datas\*" "%AGENT_DIST_PATH%\"
copy /Y "%DLPUPDATER_BUILD_PATH%\NC.exe" "%AGENT_DIST_PATH%\dlpupdater.exe"
EXIT /B /0

:CONFIGURE_INSTALLER
@REM Copy static files
xcopy /A /E /H /Y /F "%DOWNLOAD_PATH%\ghostscript\" "%AGENT_RUNTIMES_PATH%\tools\"
xcopy /A /E /H /Y /F "%DOWNLOAD_PATH%\tessdata\" "%AGENT_RUNTIMES_PATH%\tessdata\"
xcopy /A /E /H /Y /F "%DOWNLOAD_PATH%\MyKeys\*" "%AGENT_RUNTIMES_PATH%\"

copy /Y "%DOWNLOAD_PATH%\proxy.exe" "%AGENT_RUNTIMES_PATH%"
copy /Y "%DOWNLOAD_PATH%\dotnet-runtime\aspnetcore-runtime-5.0.1-win-x64.exe" "%AGENT_RUNTIMES_PATH%"
copy /Y "%DOWNLOAD_PATH%\dotnet-runtime\dotnet-runtime-5.0.1-win-x64.exe" "%AGENT_RUNTIMES_PATH%"

copy /Y "%DOWNLOAD_PATH%\drivers\dlpminifilterkernel\dlpminifilterkernel.cat" "%AGENT_RUNTIMES_PATH%"
copy /Y "%DOWNLOAD_PATH%\drivers\dlpminifilterkernel\dlpminifilterkernel.inf" "%AGENT_RUNTIMES_PATH%"
copy /Y "%DOWNLOAD_PATH%\drivers\dlpminifilterkernel\dlpminifilterkernel.sys" "%AGENT_RUNTIMES_PATH%"
copy /Y "%DOWNLOAD_PATH%\drivers\dlpminifilterkernel_win7\dlpminifilterkernel.sys" "%AGENT_RUNTIMES_PATH%\dlpminifilterkernel_win7.sys"

move /Y "%AGENT_DIST_PATH%\vc_redist.x64.exe" "%AGENT_RUNTIMES_PATH%\"
move /Y "%AGENT_DIST_PATH%\opengl32sw.dll" "%AGENT_RUNTIMES_PATH%\"

@REM TODO: Burda alt klasorleri kopyalama islemi gerekli yaparsan
@REM FOR /f "tokens=*" %%A in ('dir /b /s /a-d "%AGENT_DIST_PATH%\*"') do (
@REM     echo "file verifying: %%A"
@REM     "%UniversalCRTSdkDir%\App Certification Kit\signtool.exe" verify /pa "%%A" 2> nul | grep "Successfully verified:" >> .\signed_files.txt
@REM )
del /F signed_files.txt
"%UniversalCRTSdkDir%\App Certification Kit\signtool.exe" verify /pa "%AGENT_DIST_PATH%\*" 2> nul | grep "Successfully verified:" > .\signed_files.txt
sed -i "s/Successfully verified: //g" .\signed_files.txt
del /F .\sed*

FOR /F "delims=" %%G IN (.\signed_files.txt) DO (
    echo "moving file: %%G"
    move /Y %%G "%AGENT_RUNTIMES_PATH%\"
)
del /F .\signed_files.txt
del /F .\signed_files.txt.bak
EXIT /B 0

:DOWNLOAD_THIRDPARTY
cd "%DOWNLOAD_PATH%"

IF EXIST .\dotnet-runtime.zip (
    echo Already exist dotnet-runtime.zip
    powershell -command "& Expand-Archive -Force .\dotnet-runtime.zip .\dotnet-runtime "
) ELSE (
    echo Downloading dotnet-runtime.zip
    curl -OL "http://mydlp:=penrZ7_&2PAWbbd@mydlp-web.sparsetechnology.com/mydlp_installer_thirdparty/dotnet-runtime.zip"
    powershell -command "& Expand-Archive -Force .\dotnet-runtime.zip .\dotnet-runtime "
)

IF EXIST .\MyKeys.zip (
    echo Already exist MyKeys.zip
    powershell -command "& Expand-Archive -Force .\MyKeys.zip .\MyKeys "
) ELSE (
    echo Downloading MyKeys.zip
    curl -OL "http://mydlp:=penrZ7_&2PAWbbd@mydlp-web.sparsetechnology.com/mydlp_installer_thirdparty/MyKeys.zip"
    powershell -command "& Expand-Archive -Force .\MyKeys.zip .\MyKeys "
)

IF EXIST .\drivers.zip (
    echo Already exist drivers.zip
    powershell -command "& Expand-Archive -Force .\drivers.zip .\ "
) ELSE (
    echo Downloading drivers.zip
    curl.exe -OL "http://mydlp:=penrZ7_&2PAWbbd@mydlp-web.sparsetechnology.com/mydlp_installer_thirdparty/drivers.zip"
    powershell -command "& Expand-Archive -Force .\drivers.zip .\ "
)

IF EXIST .\windows_exporter-0.16.0-amd64.exe (
    echo Already exist windows_exporter-0.16.0-amd64.exe
) ELSE (
    echo Downloading windows_exporter-0.16.0-amd64.exe
    curl.exe -OL "http://mydlp:=penrZ7_&2PAWbbd@mydlp-web.sparsetechnology.com/mydlp_installer_thirdparty/windows_exporter-0.16.0-amd64.exe"
)

IF EXIST .\pscp.exe (
    echo Already exist pscp.exe
) ELSE (
    echo Downloading pscp.exe
    curl.exe -OL "http://mydlp:=penrZ7_&2PAWbbd@mydlp-web.sparsetechnology.com/mydlp_installer_thirdparty/pscp.exe"
)

IF EXIST .\proxy.exe (
    echo Already exist proxy.exe
) ELSE (
    echo Downloading proxy.exe
    curl.exe -OL "http://mydlp:=penrZ7_&2PAWbbd@mydlp-web.sparsetechnology.com/mydlp_installer_thirdparty/proxy.exe"
)

IF EXIST .\ghostscript.zip (
    echo Already exist ghostscript.zip
    powershell -command "& Expand-Archive -Force .\ghostscript.zip .\ghostscript "
) ELSE (
    echo Downloading ghostscript.zip
    curl.exe -OL "http://mydlp:=penrZ7_&2PAWbbd@mydlp-web.sparsetechnology.com/mydlp_installer_thirdparty/ghostscript.zip"
    powershell -command "& Expand-Archive -Force .\ghostscript.zip .\ghostscript "
)

IF EXIST .\tessdata.zip (
    echo Already exist tessdata.zip
    powershell -command "& Expand-Archive -Force .\tessdata.zip .\tessdata "
) ELSE (
    echo Downloading tessdata.zip
    curl.exe -OL "http://mydlp:=penrZ7_&2PAWbbd@mydlp-web.sparsetechnology.com/mydlp_installer_thirdparty/tessdata.zip"
    powershell -command "& Expand-Archive -Force .\tessdata.zip .\tessdata "
)

cd "%PWD%"
EXIT /B 0

:theend