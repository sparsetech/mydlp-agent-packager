
netsh advfirewall firewall add rule name="mydlpweb" dir=in action=allow program="C:\Program Files\Comodo\MyDLP\mydlpweb.exe" enable=yes
netsh advfirewall firewall add rule name="mydlpmail" dir=in action=allow program="C:\Program Files\Comodo\MyDLP\mydlpmail.exe" enable=yes
netsh advfirewall firewall add rule name="mydlpclipboard" dir=in action=allow program="C:\Program Files\Comodo\MyDLP\mydlpclipboard.exe" enable=yes
netsh advfirewall firewall add rule name="proxy" dir=in action=allow program="C:\Program Files\Comodo\MyDLP\proxy.exe" enable=yes
netsh advfirewall firewall add rule name="NC" dir=in action=allow program="C:\Program Files\Comodo\MyDLP\NC.exe" enable=yes

certutil -addstore "Root" "C:\Program Files\Comodo\MyDLP\dlpminifilterkernel.cer"
certutil -addstore "Root" "C:\Program Files\Comodo\MyDLP\MyCertificate.crt"

@REM -----------------------------------------------------------------------------------
@ECHO ON
fltmc unload dlpminifilterkernel
sc.exe stop dlpminifilterkernel
sc.exe stop NC

taskkill /IM NC.exe /F /T
taskkill /IM mydlp* /F /T
taskkill /IM proxy.exe /F /T

sc.exe delete NC
sc.exe create NC binpath= "C:\Program Files\Comodo\MyDLP\NC.exe" start= auto
sc.exe failure NC reset= 86400 actions= restart/5000/restart/5000/restart/5000

@REM sc.exe start NC