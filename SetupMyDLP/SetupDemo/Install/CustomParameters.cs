using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration.Install;
using System.Collections;

namespace SetupDemo.Install
{
	public class CustomParameters
	{
		/// <summary>
		/// This inner class maintains the key names for the parameter values that may be passed on the 
		/// command line.
		/// </summary>
		public class Keys
		{
			public const string NCHOST = "NCHOST";
			public const string NCRUNMODE = "NCRUNMODE";
		}

		private string _NCHost = null;
		public string NCHost
		{
			get { return _NCHost; }
		}

		private string _NCRunMode = null;
		public string NCRunMode
		{
			get { return _NCRunMode; }
		}

		/// <summary>
		/// This constructor is invoked by Install class methods that have an Install Context built from 
		/// parameters specified in the command line. Rollback, Install, Commit, and intermediate methods like
		/// OnAfterInstall will all be able to use this constructor.
		/// </summary>
		/// <param name="installContext">The install context containing the command line parameters to set the strong types variables to.</param>
		public CustomParameters(InstallContext installContext)
		{
			this._NCHost = installContext.Parameters[Keys.NCHOST];
			this._NCRunMode = installContext.Parameters[Keys.NCRUNMODE];
		}

		/// <summary>
		/// This constructor is used by the Install class methods that don't have an Install Context built
		/// from the command line. This method is primarily used by the Uninstall method.
		/// </summary>
		/// <param name="savedState">An IDictionary object that contains the parameters that were saved from a prior installation.</param>
		public CustomParameters(IDictionary savedState)
		{
			if(savedState.Contains(Keys.NCHOST) == true)
				this._NCHost = (string) savedState[Keys.NCHOST];

			if (savedState.Contains(Keys.NCRUNMODE) == true)
				this._NCRunMode = (string) savedState[Keys.NCRUNMODE];
		}
	}
}
