﻿using System;
using System.Diagnostics;
using System.Configuration.Install;
using System.IO;

namespace SetupDemo.Install
{
    public class Helper
    {
        public static InstallContext context = null;

        public static void ConfigureMyDLP()
        {
            PS("certutil.exe", "-addstore \"Root\" \"C:\\Program Files\\Comodo\\MyDLP\\dlpminifilterkernel.cer\"");
            PS("certutil.exe", "-addstore \"Root\" \"C:\\Program Files\\Comodo\\MyDLP\\MyCertificate.crt\"");

            PS("sc.exe", "delete NC");
            if (Environment.OSVersion.Version.Major == 6)
                PS("sc.exe", "create NC binpath= \"C:\\Program Files\\Comodo\\MyDLP\\NC.exe\" start= delayed-auto");
            else
                PS("sc.exe", "create NC binpath= \"C:\\Program Files\\Comodo\\MyDLP\\NC.exe\" start= auto");
            PS("sc.exe", "failure NC reset= 86400 actions= restart/5000/restart/5000/restart/5000");
        }

        public static void StopMyDLP()
        {
            StopAllProcesses();
            StopOldMyDLPProcesses();
        }

        public static void StopAllProcesses()
        {
            PS("fltmc.exe", "unload dlpminifilterkernel");

            PS("sc.exe", "stop dlpminifilterkernel");
            PS("sc.exe", "stop NC");
            PS("sc.exe", "delete NC");

            PS("taskkill.exe", "/F /T /IM nc*");
            PS("taskkill.exe", "/F /T /IM mydlp*");
            PS("taskkill.exe", "/F /T /IM proxy*");
        }

        internal static void InstallRuntimes()
        {
            PS("C:\\Program Files\\Comodo\\MyDLP\\vc_redist.x64.exe", "/install /quiet /norestart");
            PS("C:\\Program Files\\Comodo\\MyDLP\\dotnet-runtime-5.0.1-win-x64.exe", "/install /quiet /norestart");
            PS("C:\\Program Files\\Comodo\\MyDLP\\aspnetcore-runtime-5.0.1-win-x64.exe", "/install /quiet /norestart");
        }

        internal static void StartMyDLP()
        {
            PS("sc.exe", "start NC");
        }

        public static void StopOldMyDLPProcesses()
        {
            PS("fltmc.exe", "unload dlpminifilterkernel");

            PS("sc.exe", "stop dlpsvc");
            PS("sc.exe", "delete dlpsvc");

            PS("sc.exe", "stop mydlpservice");
            PS("sc.exe", "delete mydlpservice");

            PS("sc.exe", "stop dlpminifilterkernel");
            PS("sc.exe", "stop windivert");

            PS("taskkill.exe", "/F /T /IM MyDLP.exe");
            PS("taskkill.exe", "/F /T /IM wizard.exe");
            PS("taskkill.exe", "/F /T /IM MyDLPEpServ.exe");

            PS("taskkill.exe", "/F /T /IM proxy.exe");
            PS("taskkill.exe", "/F /T /IM mailproxy.exe");

            PS("taskkill.exe", "/F /T /IM ap.exe");
            PS("taskkill.exe", "/F /T /IM dlpsvc.exe");
            PS("taskkill.exe", "/F /T /IM dbprcli.exe");
            PS("taskkill.exe", "/F /T /IM BackServe.exe");
            PS("taskkill.exe", "/F /T /IM self_update.exe");
            PS("taskkill.exe", "/F /T /IM tusd.exe");

            PS("taskkill.exe", "/F /T /IM lyra.exe");
            PS("taskkill.exe", "/F /T /IM exiftool.exe");
            PS("taskkill.exe", "/F /T /IM cppcryptfs.exe");
            PS("taskkill.exe", "/F /T /IM cppcryptfsctl.exe");
        }

        public static void Msg2File(string msg)
        {
            var log = $"{DateTimeOffset.Now,40}: {msg}\n";
            File.AppendAllText(Path.Combine(Path.GetTempPath(), "mydlp-setup.log"), log);
        }

        public static void PS(string exec, string args)
        {
            var pi = new ProcessStartInfo()
            {
                FileName = exec,
                Arguments = args,
                CreateNoWindow = true,
                UseShellExecute = false,
                WindowStyle = ProcessWindowStyle.Hidden
            };
            Process p = null;
            try
            {
                p = Process.Start(pi);
                p?.WaitForExit(60000);
            }
            catch (Exception e) { context?.LogMessage($"$:> {exec} {args} ==================> {e}"); }
            // Print to log
            context?.LogMessage($"$:> {exec} {args} ==================> {p?.ExitCode}");
        }
    }
}
