using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Windows.Forms;
using System.Text;
using System.Collections;

namespace SetupDemo.Install
{
    [RunInstaller(true)]
    public partial class InstallerCustomActions : Installer
    {
        private int UILevel { get; set; } = -1;

        public InstallerCustomActions()
        {
            InitializeComponent();

            // Set context
            Helper.context = this.Context;
        }

        public override void Install(IDictionary stateSaver)
        {
            if (Context.Parameters["UILevel"] != null)
            {
                if (int.TryParse(Context.Parameters["UILevel"], out int level))
                {
                    UILevel = level;
                }
            }

            // Get the custom parameters from the install context.
            CustomParameters customParameters = new CustomParameters(this.Context);
            SaveCustomParametersInStateSaverDictionary(stateSaver, customParameters);

            // Stop All MyDLP
            Helper.StopMyDLP();

            base.Install(stateSaver);
        }

        protected override void OnAfterInstall(IDictionary savedState)
        {
            // Install runtimes
            // Helper.InstallRuntimes();

            // Configure MyDLP
            Helper.ConfigureMyDLP();

            // Start NC
            Helper.StartMyDLP();

            base.OnAfterInstall(savedState);
        }

        private void SaveCustomParametersInStateSaverDictionary(IDictionary stateSaver, CustomParameters customParameters)
        {
            // Add/update the "MyCustomParameter" entry in the state saver so that it may be accessed on uninstall.
            if (stateSaver.Contains(CustomParameters.Keys.NCHOST) == true)
                stateSaver[CustomParameters.Keys.NCHOST] = customParameters.NCHost;
            else
                stateSaver.Add(CustomParameters.Keys.NCHOST, customParameters.NCHost);

            // Add/update the "MyOtherCustomParameter" entry in the state saver so that it may be accessed on uninstall.
            if (stateSaver.Contains(CustomParameters.Keys.NCRUNMODE) == true)
                stateSaver[CustomParameters.Keys.NCRUNMODE] = customParameters.NCRunMode;
            else
                stateSaver.Add(CustomParameters.Keys.NCRUNMODE, customParameters.NCRunMode);
        }

        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            // Stop All MyDLP
            Helper.StopMyDLP();

            base.OnBeforeUninstall(savedState);
        }

        public override void Uninstall(IDictionary savedState)
        {
            if (Context.Parameters["UILevel"] != null)
            {
                if (int.TryParse(Context.Parameters["UILevel"], out int level))
                {
                    UILevel = level;
                }
            }

            // Get the custom parameters from the saved state.
            CustomParameters customParameters = new CustomParameters(savedState);

            base.Uninstall(savedState);
        }

        private void PrintMessage(string message)
        {
            if (UILevel <= 3)
                return;

            MessageBox.Show(message, "Installer Custom Action Fired!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}