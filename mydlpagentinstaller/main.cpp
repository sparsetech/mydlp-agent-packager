#include <QProcess>
#include <QCoreApplication>

#include <QFile>
#include <QDateTime>
#include <QThread>
#include <QTemporaryFile>
#include <QProcess>
#include <QSettings>
#include <QNetworkReply>
#include <QElapsedTimer>
#include <QNetworkAccessManager>

static QByteArray downloadUrl(const QString &target)
{
	QNetworkAccessManager nm;
	QUrl url(target);
	QNetworkRequest req(url);
	req.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
	QEventLoop el;
	QObject::connect(&nm, &QNetworkAccessManager::finished, &el, &QEventLoop::quit);
	auto *reply = nm.get(req);
	el.exec();
	if (reply->error()) {
		printf("some error occurred during download\n");
		return QByteArray();
	}
	return reply->readAll();
}

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	QString cloudip = "1.1.1.1";
	if (a.arguments().size() < 2) {
		printf("mydlpagentinstaller.exe {cloudip}\n");
		return -ENOENT;
	}
	cloudip = a.arguments().at(1);

	{
		QSettings sets("HKEY_LOCAL_MACHINE\\SOFTWARE\\SparseTech\\NC\\", QSettings::Registry64Format);
		sets.setValue("NCCloudHost", cloudip);
	}
	{
		QSettings sets("HKEY_LOCAL_MACHINE\\SOFTWARE\\SparseTech\\NC\\", QSettings::Registry32Format);
		sets.setValue("NCCloudHost", cloudip);
	}

	/*QTemporaryFile temp;
	if (!temp.open()) {
		printf("cannot open a temp file for writing");
	}
	temp.close();*/
	QString fname = "installer.exe";

	if (a.arguments().size() > 2) {
		auto url = a.arguments().at(2);
		printf("downloading update file '%s'\n", qPrintable(url)); fflush(stdout);
		auto ba = downloadUrl(url);
		if (!ba.size()) {
			printf("error downloading url '%s'\n", qPrintable(url));
			return -EINVAL;
		}
		QFile f(fname);
		if (!f.open(QIODevice::WriteOnly)) {
			printf("error writing downloaded file, please check your permissions\n");
			return -EPERM;
		}
		f.write(ba);
		f.close();
	} else {
		QByteArray ba;
		{
			QFile f(":/mydlp_setup.exe");
			if (!f.open(QIODevice::ReadOnly)) {
				printf("error reading builtin installer\n");
				return -EIO;
			}
			ba = f.readAll();
			f.close();
		}
		QFile f(fname);
		if (!f.open(QIODevice::WriteOnly)) {
			printf("error extracting installer exe\n");
			return -EINVAL;
		}
		f.write(ba);
	}

	printf("starting installer exe, this may take some while...\n");

	QProcess p;
	p.setProgram(fname);
	p.setArguments(QStringList() << "-y");
	p.start();
	p.waitForStarted();
	p.waitForFinished();
	printf("installation completed with error '%d'\n", p.exitCode());

	return 0;
}
